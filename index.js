const express = require("express") 
const cors = require('cors')
const dotenv = require ('dotenv')
const router = require("./src/routes/main")
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./src/docs/swagger-api-docs.json');


// get config env
dotenv.config();

const app = express()
const port = 8070

const options = {
  customCssUrl: 'https://unpkg.com/swagger-ui-dist@4.5.0/swagger-ui.css'
}


var corsOptions = {
  origin: '*', // ini merupakan host frontend yang nantinya akan menggunakan backend ini
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions))
app.use(express.json())
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options)); 
app.use("/", router)

app.listen(port, () => {
  console.log(`app listening on port ${port}`)
})
